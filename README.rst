Architecture Docker du prototype WattSim
========================================

Cette architecture est très incomplète pour un déploiement sûr en production
mais permet une démonstration du bon fonctionnement des composants de base
de l'application.

.. contents:: **Sommaire**

Pré-requis
----------

* *Recommandé :* Ubuntu 16.04 ou ultérieur
* Docker (`instructions d'installation <https://docs.docker.com/install/>`_)
* Docker Compose (`instructions <https://docs.docker.com/compose/install/>`_)

Première exécution
------------------

Toutes les commandes sont à exécuter à l'intérieur du clone de ce dépôt Git.

On commence par démarrer l'architecture normalement : ::

   docker-compose -p wattsim up

Dans un autre terminal, on va demander au backend d'initialiser sa base
de données : ::

   docker exec wattsim-backend manage.py migrate

Ensuite, on collecte les fichiers statiques de l'administration de Django,
juste pour avoir une interface un peu plus jolie : ::

   docker exec wattsim-backend manage.py collectstatic --noinput

Enfin, on va créer un administrateur pour pouvoir gérer le backend depuis une
interface web et se connecter au frontend : ::

   docker exec -it wattsim-backend manage.py createsuperuser

Le site devient alors disponible sur le port 80 de la machine locale :

* http://localhost/: Page d'accueil du front-end
* http://localhost/admin/: Interface d'administration

Deux conteneurs sont démarrés :

* ``wattsim-backend``, le backend Django et la base de données SQLite ;
* ``wattsim-frontend``, le frontend Vue.js.

Exécutions suivantes
--------------------

Si la base de données est déjà en place et qu'un administrateur existe déjà,
tout ce qu'il y a à faire est de démarrer l'architecture : ::

   docker-compose -p wattsim up

Envoi de données
----------------

Utilisation du simulateur
^^^^^^^^^^^^^^^^^^^^^^^^^

::

   docker run -it --network=wattsim_default registry.gitlab.com/wattsim/simulator:latest

Le script vous demandera le nom d'utilisateur et le mot de passe du compte
et enverra toutes les dix secondes de nouvelles données. Pour envoyer des
données plus fréquemment, ajouter ``--frequency [x]`` à la ligne,
où ``[x]`` est le nombre de secondes entre chaque envoi.

Avec une vraie station
^^^^^^^^^^^^^^^^^^^^^^

Consultez les instructions d'installation et d'utilisation du Sender,
le script Python permettant d'envoyer des données provenant d'une vraie
station. https://gitlab.com/wattsim/sender

Mise à jour
-----------

Si l'application a subi des mises à jour depuis l'installation sur la machine,
il est possible de mettre à jour les images Docker : ::

   docker-compose -p wattsim pull
   docker pull registry.gitlab.com/wattsim/simulator:latest

On peut ensuite de nouveau démarrer l'architecture normalement et les images
plus récentes seront utilisées.

Construction depuis les dépôts Git
----------------------------------

Pour construire soi-même les images Docker sans compter sur celles déjà
disponibles sur GitLab CI, en disposant de clones des dépôts ``backend``,
``frontend``, et ``simulator``, ainsi que d'une installation de npm et
Node.js 10 ou ultérieur : ::

   cd ../backend
   docker build -t registry.gitlab.com/wattsim/backend:latest .
   cd ../frontend
   npm i
   npm run build
   docker build -t registry.gitlab.com/wattsim/frontend:latest .
   cd ../simulator
   docker build -t registry.gitlab.com/wattsim/simulator:latest .

On peut ensuite démarrer aussitôt l'architecture.
